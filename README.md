README
======

A program to parse and validate credit cards using the Luhn algorithm.

## Installation

Clone this repository:

```
git clone https://bitbucket.org/ismailfaruqi/luhn.git
```

## Running the program

Given that the cloned repository are saved into __luhn__ directory:

```
cd luhn
ruby luhn.rb
```
